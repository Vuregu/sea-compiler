import sys
import antlr4
from SeaLexer import SeaLexer
from SeaParser import SeaParser
from SeaVisitor import SeaVisitor
from SeaCompiler import SeaCompiler
import SeaSemantics
import SeaException

for file_name in sys.argv[1:]:

    try:
        file = antlr4.FileStream(file_name)
        lexer = SeaLexer(file)
        lexer.removeErrorListeners()
        lexer.addErrorListener(SeaException.SeaErrorListener())
        stream = antlr4.CommonTokenStream(lexer)
        parser = SeaParser(stream)
        parser.removeErrorListeners()
        parser.addErrorListener(SeaException.SeaErrorListener())
        concrete_tree = parser.program()
        abstract_tree = SeaVisitor().visitProgram(concrete_tree)
        symbol_table = SeaSemantics.build_symbol_table(abstract_tree)
        SeaSemantics.traverse_tree(abstract_tree, symbol_table)
        compiled_file = open(file.fileName.replace(".c", ".p"), "w")
        compiler = SeaCompiler(abstract_tree, symbol_table, compiled_file)
        compiler.start_compilation()

    except FileNotFoundError as error:
        print("Specified file: '" + file_name + "' does not exist.", file=sys.stderr)
    except SeaException.LexicalError as error:
        print(file.fileName + " | " + str(error), file=sys.stderr)
    except SeaException.ParsingError as error:
        print(file.fileName + " | " + str(error), file=sys.stderr)
    except SeaException.SemanticError as error:
        print(file.fileName + " | " + str(error), file=sys.stderr)
    else:
        print(file.fileName + " | " + "Successfully compiled.")

