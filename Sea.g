grammar Sea;

options {
    language=Python3;
}

program:							( variable_declarations | function_prototype | function | include )* EOF ;
include:							'#include' '<' ( STRING | 'stdio.h' ) '>' ;
function:							type_specifier function_declaration compound_statement ;
function_prototype:					type_specifier function_declaration ( ',' function_declaration )* ';' ;
function_declaration:				pointer* ID '(' parameter_declarations? ')' ;
type_specifier:						data_type | 'const' data_type | data_type 'const' ;
data_type:							'int' | 'char' | 'float' | 'void' ;
pointer:							'*' 'const'? ;
array_declaration:					'[' ( expression )? ']' ;
array_dereference:					'[' ( expression ) ']' ;
element_list:						'{' ( expression ( ',' expression )* )? '}' ;
parameter_declarations:				parameter_declaration ( ',' parameter_declaration )* ;
parameter_declaration:				type_specifier pointer* ID? array_declaration? ;
compound_statement:					'{' ( variable_declarations | function_prototype | function | include | statement )* '}' ;
variable_declarations:				type_specifier variable_declaration ( ',' variable_declaration )* ';' ;
variable_declaration:				pointer* ID array_declaration? ( '=' ( expression | element_list ) )? ;
statement:							compound_statement | conditional_statement | for_statement | while_statement | do_while_statement | expression_statement | return_statement ;
conditional_statement:				'if' '(' expression ')' statement ( 'else' statement )? ;
for_statement:						'for' '(' for_statement_initializer for_statement_condition for_statement_increment? ')' statement ;
for_statement_initializer:			expression ';' | variable_declarations ;
for_statement_condition:			expression ';' ;
for_statement_increment:			expression ;
while_statement:					'while' '(' expression ')' statement ;
do_while_statement:					'do' statement 'while' '(' expression ')' ';' ;
expression_statement:				expression ';' ;
return_statement:					'return' expression? ';' ;
expression:							condition ( '=' expression )? ;
condition:							disjunction ( '?' expression ':' condition )? ;
disjunction:						conjunction | disjunction '||' conjunction ;
conjunction:						relation | conjunction '&&' relation ;
relation:							addition ( '==' addition | '!=' addition | '>' addition | '<' addition | '>=' addition | '<=' addition )? ;
addition:							multiplication | addition '+' multiplication | addition '-' multiplication ;
multiplication:						factor | multiplication '*' factor | multiplication '/' factor | multiplication '%' factor ;
factor:								secondary | '!' factor | '-' factor | '++' factor | '--' factor | '&' factor | '*' factor ;
secondary:							primary | secondary '++' | secondary '--' | secondary array_dereference ;
primary:							INT | CHAR | FLOAT | STRING | ID | function_call | '(' expression ')' ;
function_call:						ID '(' ( expression ( ',' expression )* )? ')' ;

FLOAT:								[0-9]+ '.' [0-9]+ | [0-9]+ '.' | '.' [0-9]+ ;
INT:								[0-9]+ ;

CHAR:								'\'' ( [a-z] | [A-Z] | [0-9] | ' ' | '\t' |
											'~' | '%' | '|' | '@' | '+' | '<' | '_' | '-' | '>' | '^' | '#' | '=' | '&' | '$' | '/' | '(' | '*' | ')' | ':' | '[' | '"' | ';' | ']' | '!' | ',' | '{' | '?' | '.' | '}' |
											'\\b' | '\\t' | '\\v' | '\\r' | '\\f' | '\\n' | '\\\\' | '\\�' | '\\"' | '\\?' | '\\0' | '\\a' )+
									'\'' ;

STRING:								'"' ( [a-z] | [A-Z] | [0-9] | ' ' | '\t' |
										   '~' | '%' | '|' | '@' | '+' | '<' | '_' | '-' | '>' | '^' | '#' | '=' | '&' | '$' | '/' | '(' | '*' | ')' | ':' | '[' | '\'' | ';' | ']' | '!' | ',' | '{' | '?' | '.' | '}' |
										   '\\b' | '\\t' | '\\v' | '\\r' | '\\f' | '\\n' | '\\\\' | '\\�' | '\\"' | '\\?' | '\\0' | '\\a' )*
									'"' ;

ID: 								( [a-z] | [A-Z] | '_' ) ( [a-z] | [A-Z] | [0-9] | '_' )* ;
SINGLE_LINE_COMMENT	:				'//' .*? [\r\n]+ -> skip ;
MULTI_LINE_COMMENT	:				'/*' .*? '*/' -> skip ;
WS : 								[ \t\r\n]+ -> skip ;
