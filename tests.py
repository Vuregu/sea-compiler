import sys
import antlr4
from SeaLexer import SeaLexer
from SeaParser import SeaParser
from SeaVisitor import SeaVisitor
from SeaCompiler import SeaCompiler
import SeaSemantics
import SeaException

working_test_dir = "working_tests/"
faulty_test_dir = "faulty_tests/"
test_programs = ["add.c", "addn.c", "array.c", "bubble.c", "factor.c", "factorial.c", "line.c", "linear.c", "prime.c", "randompermute.c", "scope.c", "selection.c"]

def run_working_tests():
    for program in test_programs:
        file = antlr4.FileStream(working_test_dir + program)
        try:
            lexer = SeaLexer(file)
            lexer.removeErrorListeners()
            lexer.addErrorListener(SeaException.SeaErrorListener())
            stream = antlr4.CommonTokenStream(lexer)
            parser = SeaParser(stream)
            parser.removeErrorListeners()
            parser.addErrorListener(SeaException.SeaErrorListener())
            concrete_tree = parser.program()
            abstract_tree = SeaVisitor().visitProgram(concrete_tree)
            symbol_table = SeaSemantics.build_symbol_table(abstract_tree)
            SeaSemantics.traverse_tree(abstract_tree, symbol_table)
            compiled_file = open(file.fileName.replace(".c", ".p"), "w")
            compiler = SeaCompiler(abstract_tree, symbol_table, compiled_file)
            compiler.start_compilation()

        except SeaException.LexicalError as error:
            print(file.fileName + " | " + str(error), file=sys.stderr)
        except SeaException.ParsingError as error:
            print(file.fileName + " | " + str(error), file=sys.stderr)
        except SeaException.SemanticError as error:
            print(file.fileName + " | " + str(error), file=sys.stderr)
        else:
            print(file.fileName + " | " + "Successfully compiled.")

def run_faulty_tests():
    for program in test_programs:
        file = antlr4.FileStream(faulty_test_dir + program)
        try:
            lexer = SeaLexer(file)
            lexer.removeErrorListeners()
            lexer.addErrorListener(SeaException.SeaErrorListener())
            stream = antlr4.CommonTokenStream(lexer)
            parser = SeaParser(stream)
            parser.removeErrorListeners()
            parser.addErrorListener(SeaException.SeaErrorListener())
            concrete_tree = parser.program()
            abstract_tree = SeaVisitor().visitProgram(concrete_tree)
            symbol_table = SeaSemantics.build_symbol_table(abstract_tree)
            SeaSemantics.traverse_tree(abstract_tree, symbol_table)
            compiled_file = open(file.fileName.replace(".c", ".p"), "w")
            compiler = SeaCompiler(abstract_tree, symbol_table, compiled_file)
            compiler.start_compilation()

        except SeaException.LexicalError as error:
            print(file.fileName + " | " + str(error), file=sys.stderr)
        except SeaException.ParsingError as error:
            print(file.fileName + " | " + str(error), file=sys.stderr)
        except SeaException.SemanticError as error:
            print(file.fileName + " | " + str(error), file=sys.stderr)
        else:
            print(file.fileName + " | " + "Successfully compiled.")

# /* Uncomment the line below to run the working test files, they should not report any lexical, syntactical or semantic errors. */
run_working_tests()

# /* Uncomment the line below to run the faulty test files, they should report lexical, syntactical and semantic errors as specified in the readme. */
run_faulty_tests()

