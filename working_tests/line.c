/* line.c -- It reads lines from input and echoes them back.
 */

#include <stdio.h>

int main(void) {
  char c;
  int count;

  for(0;1;){
    count=0;
    printf("Please enter a line [blank line to terminate]> ");
    do{
      scanf("%c", &c);
      count++;
    }while (c!='\n');
    if(count==1)return;
  }
}
