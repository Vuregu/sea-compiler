import re
from SeaNodes import *

class SeaCompiler:

    def __init__(self, abstract_tree, symbol_table, file):
        self.abstract_tree = abstract_tree
        self.symbol_table = symbol_table
        self.file = file
        self.latest_label_number = 0

    def start_compilation(self):

        self.file.write("sep 1000" + "\n")
        for statement in self.abstract_tree.global_statements:
            if type(statement) is VariableDeclarationsNode:
                for variable_declaration in statement.variable_declarations:
                    self.compile_variable_declaration(variable_declaration)
        self.evaluate_expression(FunctionCallNode("main"))
        self.file.write("hlt" + "\n")

        for statement in self.abstract_tree.global_statements:
            if type(statement) is FunctionNode:
                self.compile_function(statement)

    def compile_variable_declaration(self, variable_declaration):
        if variable_declaration.assignment is None:

            if variable_declaration.type.array_declaration is None:
                self.file.write("ldc " + variable_declaration.type.p_machine_type() + " " + variable_declaration.type.p_machine_default_value() + "\n")
            else:
                p_machine_array_type = variable_declaration.type.p_machine_type()
                p_machine_default_array_value = variable_declaration.type.p_machine_default_value()

                for i in range(variable_declaration.type.array_declaration.size.primary):
                    self.file.write("ldc " + p_machine_array_type + " " + p_machine_default_array_value + "\n")
        else:

            if variable_declaration.type.array_declaration is None:
                self.evaluate_expression(variable_declaration.assignment)
            else:
                p_machine_array_type = variable_declaration.type.p_machine_type()
                p_machine_default_array_value = variable_declaration.type.p_machine_default_value()
                element_count = len(variable_declaration.assignment.elements)

                for i in range(variable_declaration.type.array_declaration.size.primary):
                    if i < element_count:
                        self.evaluate_expression(variable_declaration.assignment.elements[i])
                    else:
                        self.file.write("ldc " + p_machine_array_type + " " + p_machine_default_array_value + "\n")

    def compile_function(self, function):
        procedure_name = self.symbol_table.resolve(function.id).procedure_name

        self.file.write("ujp " + procedure_name  + "_end" + "\n")
        self.file.write(procedure_name + ":" + "\n")
        self.file.write("sep 1000" + "\n")
        self.compile_compound_statement(function.compound_statement, True)

        if function.type.data_type.data_type == "void":
            self.file.write("retp" + "\n")
        else:
            self.file.write("ldc " + function.type.p_machine_type() + " " + function.type.p_machine_default_value() + "\n")
            self.file.write("str " + function.type.p_machine_type() + " 0 0" + "\n")
            self.file.write("retf" + "\n")

        self.file.write(procedure_name + "_end:" + "\n")

    def compile_compound_statement(self, compound_statement, parent_function = False):
        parent_symbol_table = self.symbol_table
        self.symbol_table = self.symbol_table.get_child(compound_statement)

        local_variable_count = 0
        for statement in compound_statement.local_statements:
            if type(statement) is VariableDeclarationsNode:
                for variable_declaration in statement.variable_declarations:
                    self.compile_variable_declaration(variable_declaration)
                    local_variable_count += 1

        for statement in compound_statement.local_statements:
            if type(statement) is not VariableDeclarationsNode:
                self.compile_statement(statement)

        if parent_function is False and local_variable_count > 0:
            self.file.write("ssp " + str(parent_symbol_table.next_variable_offset()) + "\n")

        self.symbol_table = parent_symbol_table

    def compile_conditional_statement(self, statement):
        if_end_label = self.get_unique_label_name("if_end")

        if statement.statement_else is None:

            self.evaluate_expression(statement.condition)
            self.file.write("fjp " + if_end_label + "\n")
            self.compile_statement(statement.statement)
            self.file.write(if_end_label + ":" + "\n")
        else:
            if_else_label = self.get_unique_label_name("if_else")

            self.evaluate_expression(statement.condition)
            self.file.write("fjp " + if_else_label + "\n")
            self.compile_statement(statement.statement)
            self.file.write("ujp " + if_end_label + "\n")
            self.file.write(if_else_label + ":" + "\n")
            self.compile_statement(statement.statement_else)
            self.file.write(if_end_label + ":" + "\n")

    def compile_for_statement(self, statement):
        for_label = self.get_unique_label_name("for")
        for_end_label = self.get_unique_label_name("for_end")

        if type(statement.initializer) is VariableDeclarationsNode:
            parent_symbol_table = self.symbol_table
            self.symbol_table = self.symbol_table.get_child(statement.initializer)

            for variable_declaration in statement.initializer.variable_declarations:
                self.compile_variable_declaration(variable_declaration)
            self.file.write(for_label + ":" + "\n")
            self.evaluate_expression(statement.condition)
            self.file.write("fjp " + for_end_label + "\n")
            self.compile_statement(statement.statement)
            if statement.increment is not None:
                self.evaluate_expression(statement.increment)
                self.file.write("ssp " + str(self.symbol_table.next_variable_offset()) + "\n")
            self.file.write("ujp " + for_label + "\n")
            self.file.write(for_end_label + ":" + "\n")

            self.file.write("ssp " + str(parent_symbol_table.next_variable_offset()) + "\n")
            self.symbol_table = parent_symbol_table

        else:
            self.evaluate_expression(statement.initializer)
            self.file.write("ssp " + str(self.symbol_table.next_variable_offset()) + "\n")
            self.file.write(for_label + ":" + "\n")
            self.evaluate_expression(statement.condition)
            self.file.write("fjp " + for_end_label + "\n")
            self.compile_statement(statement.statement)
            if statement.increment is not None:
                self.evaluate_expression(statement.increment)
                self.file.write("ssp " + str(self.symbol_table.next_variable_offset()) + "\n")
            self.file.write("ujp " + for_label + "\n")
            self.file.write(for_end_label + ":" + "\n")

    def compile_while_statement(self, statement):
        while_label = self.get_unique_label_name("while")
        while_end_label = self.get_unique_label_name("while_end")

        self.file.write(while_label + ":" + "\n")
        self.evaluate_expression(statement.condition)
        self.file.write("fjp " + while_end_label + "\n")
        self.compile_statement(statement.statement)
        self.file.write("ujp " + while_label + "\n")
        self.file.write(while_end_label + ":" + "\n")

    def compile_do_while_statement(self, statement):
        while_label = self.get_unique_label_name("while")
        while_skip_condition_label = self.get_unique_label_name("while_skip_condition")
        while_end_label = self.get_unique_label_name("while_end")

        self.file.write("ujp " + while_skip_condition_label + "\n")
        self.file.write(while_label + ":" + "\n")
        self.evaluate_expression(statement.condition)
        self.file.write("fjp " + while_end_label + "\n")
        self.file.write(while_skip_condition_label + ":" + "\n")
        self.compile_statement(statement.statement)
        self.file.write("ujp " + while_label + "\n")
        self.file.write(while_end_label + ":" + "\n")

    def compile_expression_statement(self, statement):
        self.evaluate_expression(statement.expression)
        self.file.write("ssp " + str(self.symbol_table.next_variable_offset()) + "\n")

    def compile_return_statement(self, statement):
        if statement.expression is not None:
            self.evaluate_expression(statement.expression)
            self.file.write("str " + self.calculate_type(statement.expression).p_machine_type() + " 0 0" + "\n")
            self.file.write("retf" + "\n")
        else:
            self.file.write("retp" + "\n")

    def compile_statement(self, statement):
        if type(statement) is CompoundStatementNode:
            self.compile_compound_statement(statement)

        elif type(statement) is FunctionNode:
            self.compile_function(statement)

        elif type(statement) is ConditionalStatementNode:
            self.compile_conditional_statement(statement)

        elif type(statement) is ForStatementNode:
            self.compile_for_statement(statement)

        elif type(statement) is WhileStatementNode:
            self.compile_while_statement(statement)

        elif type(statement) is DoWhileStatementNode:
            self.compile_do_while_statement(statement)

        elif type(statement) is ExpressionStatementNode:
            self.compile_expression_statement(statement)

        else: # if type(statement) is ReturnStatementNode
            self.compile_return_statement(statement)

    def calculate_type(self, expression):
        if type(expression) is ExpressionNode:
            return self.calculate_type(expression.condition)

        elif type(expression) is ConditionNode:
            return self.calculate_type(expression.ternary_true_expression)

        elif type(expression) is DisjunctionNode:
            return TypeNode(DataTypeNode("bool"))

        elif type(expression) is ConjunctionNode:
            return TypeNode(DataTypeNode("bool"))

        elif type(expression) is RelationNode:
            return TypeNode(DataTypeNode("bool"))

        elif type(expression) is AdditionNode:
            return self.calculate_type(expression.with_multiplication)

        elif type(expression) is MultiplicationNode:
            return self.calculate_type(expression.with_factor)

        elif type(expression) is FactorNode:
            if expression.operation == "!":
                return TypeNode(DataTypeNode("bool"))

            elif expression.operation == "-" or expression.operation == "++" or expression.operation == "--":
                return self.calculate_type(expression.factor)

            elif expression.operation == "&":
                if type(expression.factor) is PrimaryNode:
                    variable_type = self.symbol_table.resolve(expression.factor.primary).symbol_type.copy_type()
                    variable_type.pointer.append(PointerNode(False))
                    return variable_type
                else:
                    dereferenced_type = self.calculate_type(expression.factor)
                    dereferenced_type.pointers.append(PointerNode(False))
                    return dereferenced_type

            else: # if dereference
                pointer_type = self.calculate_type(expression.factor)
                pointer_type.pointers.pop()
                return pointer_type

        elif type(expression) is SecondaryNode:
            if expression.operation == "++" or expression.operation == "--":
                return self.calculate_type(expression.factor)

            else: # if array dereference
                pointer_type = self.calculate_type(expression.secondary)
                pointer_type.pointers.pop()
                return pointer_type

        elif type(expression) is PrimaryNode:
            if expression.type == "id":
                return self.symbol_table.resolve(expression.primary).symbol_type.copy_type()
            else:
                return TypeNode(DataTypeNode(expression.type))

        else: # if type(expression) is FunctionCallNode
            return self.symbol_table.resolve(expression.primary).symbol_type.copy_type()

    def evaluate_expression(self, expression):
        if type(expression) is ExpressionNode:
            self.evaluate_expression_node(expression)

        elif type(expression) is ConditionNode:
            self.evaluate_condition_node(expression)

        elif type(expression) is DisjunctionNode:
            self.evaluate_disjunction_node(expression)

        elif type(expression) is ConjunctionNode:
            self.evaluate_conjunction_node(expression)

        elif type(expression) is RelationNode:
            self.evaluate_relation_node(expression)

        elif type(expression) is AdditionNode:
            self.evaluate_addition_node(expression)

        elif type(expression) is MultiplicationNode:
            self.evaluate_multiplication_node(expression)

        elif type(expression) is FactorNode:
            self.evaluate_factor_node(expression)

        elif type(expression) is SecondaryNode:
            self.evaluate_secondary_node(expression)

        elif type(expression) is PrimaryNode:
            self.evaluate_primary_node(expression)

        else: # if type(expression) is FunctionCallNode
            self.evaluate_function_call_node(expression)

    def evaluate_expression_node(self, expression):
        if expression.expression_assignment is not None:

            if type(expression.condition) is PrimaryNode:
                self.evaluate_expression(expression.expression_assignment)
                variable = self.symbol_table.resolve(expression.condition.primary)
                self.file.write("str " + variable.symbol_type.p_machine_type() + " " + str(variable.relative_depth(self.symbol_table)) + " " + str(variable.offset) + "\n")
                self.file.write("lod " + variable.symbol_type.p_machine_type() + " " + str(variable.relative_depth(self.symbol_table)) + " " + str(variable.offset) + "\n")

            elif type(expression.condition) is FactorNode:
                self.evaluate_expression(expression.condition.factor)
                self.evaluate_expression(expression.condition.factor)

                self.evaluate_expression(expression.expression_assignment)
                value_type = self.calculate_type(expression.expression_assignment)
                self.file.write("sto " + value_type.p_machine_type() + "\n")
                self.file.write("ind " + value_type.p_machine_type() + "\n")

            else:
                self.evaluate_expression(expression.condition.secondary)
                self.file.write("conv a i" + "\n")
                self.evaluate_expression(expression.condition.operation.offset)
                self.file.write("add i" + "\n")
                self.file.write("conv i a" + "\n")

                self.evaluate_expression(expression.condition.secondary)
                self.file.write("conv a i" + "\n")
                self.evaluate_expression(expression.condition.operation.offset)
                self.file.write("add i" + "\n")
                self.file.write("conv i a" + "\n")

                self.evaluate_expression(expression.expression_assignment)
                value_type = self.calculate_type(expression.expression_assignment)
                self.file.write("sto " + value_type.p_machine_type() + "\n")
                self.file.write("ind " + value_type.p_machine_type() + "\n")

    def evaluate_condition_node(self, expression):
        label_ternary_false = self.get_unique_label_name("ternary_false")
        label_ternary_end = self.get_unique_label_name("ternary_end")

        self.evaluate_expression(expression.disjunction)
        self.file.write("fjp " + label_ternary_false + "\n")
        self.evaluate_expression(expression.ternary_true_expression)
        self.file.write("ujp " + label_ternary_end + "\n")
        self.file.write(label_ternary_false + ":" + "\n")
        self.evaluate_expression(expression.ternary_false_condition)
        self.file.write(label_ternary_end + ":" + "\n")

    def evaluate_disjunction_node(self, expression):
        self.evaluate_expression(expression.disjunction)
        self.evaluate_expression(expression.conjunction)
        self.file.write("or" + "\n")

    def evaluate_conjunction_node(self, expression):
        self.evaluate_expression(expression.conjunction)
        self.evaluate_expression(expression.relation)
        self.file.write("and" + "\n")

    def evaluate_relation_node(self, expression):
        comparison_type = self.calculate_type(expression.addition)

        self.evaluate_expression(expression.addition)
        self.evaluate_expression(expression.with_addition)

        if expression.relation == "==":
            self.file.write("equ " + comparison_type.p_machine_type() + "\n")
        elif expression.relation == "!=":
            self.file.write("neq " + comparison_type.p_machine_type() + "\n")
        elif expression.relation == ">":
            self.file.write("grt " + comparison_type.p_machine_type() + "\n")
        elif expression.relation == "<":
            self.file.write("les " + comparison_type.p_machine_type() + "\n")
        elif expression.relation == ">=":
            self.file.write("geq " + comparison_type.p_machine_type() + "\n")
        else: # if expression.relation == "<="
            self.file.write("leq " + comparison_type.p_machine_type() + "\n")

    def evaluate_addition_node(self, expression):
        addition_type = self.calculate_type(expression.with_multiplication)

        self.evaluate_expression(expression.addition)
        self.evaluate_expression(expression.with_multiplication)

        if expression.operation == "+":
            self.file.write("add " + addition_type.p_machine_type() + "\n")
        else: # if expression.operation == "-"
            self.file.write("sub " + addition_type.p_machine_type() + "\n")

    def evaluate_multiplication_node(self, expression):
        multiplication_type = self.calculate_type(expression.with_factor)

        self.evaluate_expression(expression.multiplication)
        self.evaluate_expression(expression.with_factor)

        if expression.operation == "*":
            self.file.write("mul " + multiplication_type.p_machine_type() + "\n")
        else: # if expression.operation == "/"
            self.file.write("div " + multiplication_type.p_machine_type() + "\n")

    def evaluate_factor_node(self, expression):
        if expression.operation == "!":
            self.evaluate_expression(expression.factor)
            self.file.write("not" + "\n")

        elif expression.operation == "-":
            self.evaluate_expression(expression.factor)
            self.file.write("neg " + self.calculate_type(expression.factor).p_machine_type() + "\n")

        elif expression.operation == "++" or expression.operation == "--":
            if type(expression.factor) is PrimaryNode:
                variable = self.symbol_table.resolve(expression.factor.primary)

                self.evaluate_expression(expression.factor)
                self.file.write("ldc " + variable.symbol_type.p_machine_type() + " " + variable.symbol_type.p_machine_unit_value() + "\n")
                self.file.write(("add " if expression.operation == "++" else "sub ") + variable.symbol_type.p_machine_type() + "\n")
                self.file.write("str " + variable.symbol_type.p_machine_type() + " " + str(variable.relative_depth(self.symbol_table)) + " " + str(variable.offset) + "\n")
                self.evaluate_expression(expression.factor)

            elif type(expression.factor) is FactorNode:
                variable_type = self.calculate_type(expression.factor)

                self.evaluate_expression(expression.factor.factor)
                self.evaluate_expression(expression.factor)
                self.file.write("ldc " + variable_type.p_machine_type() + " " + variable_type.p_machine_unit_value() + "\n")
                self.file.write(("add " if expression.operation == "++" else "sub ") + variable_type.p_machine_type() + "\n")
                self.file.write("sto " + variable_type.p_machine_type() + "\n")
                self.evaluate_expression(expression.factor)

            else:
                variable_type = self.calculate_type(expression.factor)

                self.evaluate_expression(expression.factor.secondary)
                self.file.write("conv a i" + "\n")
                self.evaluate_expression(expression.factor.operation.offset)
                self.file.write("add i" + "\n")
                self.file.write("conv i a" + "\n")

                self.evaluate_expression(expression.factor)
                self.file.write("ldc " + variable_type.p_machine_type() + " " + variable_type.p_machine_unit_value() + "\n")
                self.file.write(("add " if expression.operation == "++" else "sub ") + variable_type.p_machine_type() + "\n")
                self.file.write("sto " + variable_type.p_machine_type() + "\n")
                self.evaluate_expression(expression.factor)

        elif expression.operation == "&":
            if type(expression.factor) is PrimaryNode:
                variable = self.symbol_table.resolve(expression.factor.primary)
                self.file.write("lda " + str(variable.relative_depth(self.symbol_table)) + " " + str(variable.offset) + "\n")

            elif type(expression.condition) is FactorNode:
                self.evaluate_expression(expression.factor.factor)

            else:
                self.evaluate_expression(expression.secondary.secondary)
                self.file.write("conv a i" + "\n")
                self.evaluate_expression(expression.condition.array_dereference.offset)
                self.file.write("add i" + "\n")
                self.file.write("conv i a" + "\n")

        else: # if dereference
            self.evaluate_expression(expression.factor)
            self.file.write("ind " + self.calculate_type(expression).p_machine_type() + "\n")

    def evaluate_secondary_node(self, expression):
        if expression.operation == "++" or expression.operation == "--":
            if type(expression.secondary) is PrimaryNode:
                variable = self.symbol_table.resolve(expression.secondary.primary)

                self.evaluate_expression(expression.secondary)
                self.evaluate_expression(expression.secondary)

                self.file.write("ldc " + variable.symbol_type.p_machine_type() + " " + variable.symbol_type.p_machine_unit_value() + "\n")
                self.file.write(("add " if expression.operation == "++" else "sub ") + variable.symbol_type.p_machine_type() + "\n")
                self.file.write("str " + variable.symbol_type.p_machine_type() + " " + str(variable.relative_depth(self.symbol_table)) + " " + str(variable.offset) + "\n")

            elif type(expression.secondary) is FactorNode:
                variable_type = self.calculate_type(expression.secondary)

                self.evaluate_expression(expression.secondary)
                self.evaluate_expression(expression.secondary.factor)
                self.evaluate_expression(expression.secondary)

                self.file.write("ldc " + variable_type.p_machine_type() + " " + variable_type.p_machine_unit_value() + "\n")
                self.file.write(("add " if expression.operation == "++" else "sub ") + variable_type.p_machine_type() + "\n")
                self.file.write("sto " + variable_type.p_machine_type() + "\n")

            else:
                variable_type = self.calculate_type(expression.secondary)

                self.evaluate_expression(expression.secondary)
                self.evaluate_expression(expression.secondary.secondary)
                self.file.write("conv a i" + "\n")
                self.evaluate_expression(expression.secondary.operation.offset)
                self.file.write("add i" + "\n")
                self.file.write("conv i a" + "\n")
                self.evaluate_expression(expression.secondary)

                self.file.write("ldc " + variable_type.p_machine_type() + " " + variable_type.p_machine_unit_value() + "\n")
                self.file.write(("add " if expression.operation == "++" else "sub ") + variable_type.p_machine_type() + "\n")
                self.file.write("sto " + variable_type.p_machine_type() + "\n")

        else: # if array dereference
            self.evaluate_expression(expression.secondary)
            self.file.write("conv a i" + "\n")
            self.evaluate_expression(expression.operation.offset)
            self.file.write("add i" + "\n")
            self.file.write("conv i a" + "\n")
            self.file.write("ind " + self.calculate_type(expression).p_machine_type() + "\n")

    def evaluate_primary_node(self, expression):
        if expression.type == "id":
            variable = self.symbol_table.resolve(expression.primary)

            if variable.symbol_type.array_declaration is None:
                self.file.write("lod " + variable.symbol_type.p_machine_type() + " " + str(variable.relative_depth(self.symbol_table)) + " " + str(variable.offset) + "\n")
            else:
                self.file.write("lda " + str(variable.relative_depth(self.symbol_table)) + " " + str(variable.offset) + "\n")

        elif expression.type == "int":
            self.file.write("ldc i " + str(expression.primary) + "\n")

        elif expression.type == "float":
            self.file.write("ldc r " + str(expression.primary) + "\n")

        else: # if expression.type == "char"
            self.file.write("ldc c '" + expression.primary + "'\n")

    def evaluate_function_call_node(self, expression):
        if expression.id == "printf":
            self.compile_printf(expression)

        elif expression.id == "scanf":
            self.compile_scanf(expression)

        else:
            function = self.symbol_table.resolve(expression.id)
            self.file.write("mst " + str(function.relative_depth(self.symbol_table)) + "\n")

            for parameter in expression.parameter_list:
                self.evaluate_expression(parameter)

            self.file.write("cup " + str(len(expression.parameter_list)) + " " + function.procedure_name + "\n")

    def compile_printf(self, function_call):
        lines = function_call.parameter_list[0].primary.split("\\n")
        for index, line in enumerate(lines):
            while len(line) > 0:
                specifier = re.match("%[0-9]*[disc]", line)

                if specifier is None:
                    self.file.write("ldc c '" + line[0] + "'\n")
                    self.file.write("out c" + "\n")
                    line = line[1:]
                else:
                    self.compile_specifier(line[:specifier.end()], function_call.parameter_list.pop(1))
                    line = line[specifier.end():]

            if index != len(lines) - 1:
                self.file.write("ldc c 10" + "\n")
                self.file.write("out c" + "\n")

    def compile_specifier(self, specifier, expression):
        specifier_type = specifier[len(specifier) - 1]
        specifier_width = int(specifier[1:len(specifier) - 1] if len(specifier) > 2 else 0)

        if specifier_type == "d" or specifier_type == "i":
            if specifier_width > 1:
                int_length_division = self.get_unique_label_name("int_length_division")
                width_difference_subtraction = self.get_unique_label_name("width_difference_subtraction")
                print_int = self.get_unique_label_name("print_int")

                self.evaluate_expression(expression)
                self.file.write("ldc i " + str(specifier_width) + "\n")
                self.file.write("ldc i 0" + "\n")
                self.file.write(int_length_division + ":" + "\n")
                self.file.write("ldc i 1" + "\n")
                self.file.write("add i" + "\n")
                self.file.write("lod i 0 " + str(self.symbol_table.next_variable_offset()) + "\n")
                self.file.write("ldc i 10" + "\n")
                self.file.write("div i" + "\n")
                self.file.write("str i 0 " + str(self.symbol_table.next_variable_offset()) + "\n")
                self.file.write("lod i 0 " + str(self.symbol_table.next_variable_offset()) + "\n")
                self.file.write("ldc i 1" + "\n")
                self.file.write("les i" + "\n")
                self.file.write("fjp " + int_length_division + "\n")
                self.file.write("sub i" + "\n")

                self.file.write(width_difference_subtraction + ":" + "\n")
                self.file.write("lod i 0 " + str(self.symbol_table.next_variable_offset() + 1) + "\n")
                self.file.write("ldc i 1" + "\n")
                self.file.write("geq i" + "\n")
                self.file.write("fjp " + print_int + "\n")
                self.file.write("ldc i 1" + "\n")
                self.file.write("sub i" + "\n")
                self.file.write("ldc c 32" + "\n")
                self.file.write("out c" + "\n")
                self.file.write("ujp " + width_difference_subtraction + "\n")

                self.file.write(print_int + ":" + "\n")
                self.file.write("ssp " + str(self.symbol_table.next_variable_offset()) + "\n")

            self.evaluate_expression(expression)
            self.file.write("out i" + "\n")

        elif specifier_type == "c":
            if specifier_width > 1:
                for i in range(specifier_width - 1):
                    self.file.write("ldc c 32" + "\n")
                    self.file.write("out c" + "\n")

            self.evaluate_expression(expression)
            self.file.write("out c" + "\n")

        else: # if specifier_type == "s"
            if type(expression) is PrimaryNode and expression.type == "string":

                if specifier_width - len(expression.primary) >= 1:
                    for i in range(specifier_width - len(expression.primary)):
                        self.file.write("ldc c 32" + "\n")
                        self.file.write("out c" + "\n")

                for character in expression.primary:
                    self.file.write("ldc c '" + character + "'\n")
                    self.file.write("out c" + "\n")

            else: # if expression is pointer
                self.evaluate_expression(expression)
                self.file.write("ldc i -1" + "\n")

                read_string_label = self.get_unique_label_name("read_string_label")
                self.file.write(read_string_label + ":" + "\n")

                self.file.write("ldc i 1" + "\n")
                self.file.write("add i" + "\n")
                self.file.write("lod a 0 " + str(self.symbol_table.next_variable_offset()) + "\n")
                self.file.write("conv a i" + "\n")
                self.file.write("lod i 0 " + str(self.symbol_table.next_variable_offset() + 1) + "\n")
                self.file.write("add i" + "\n")
                self.file.write("conv i a" + "\n")
                self.file.write("ind c" + "\n")
                self.file.write("out c" + "\n")
                self.file.write("lod a 0 " + str(self.symbol_table.next_variable_offset()) + "\n")
                self.file.write("conv a i" + "\n")
                self.file.write("lod i 0 " + str(self.symbol_table.next_variable_offset() + 1) + "\n")
                self.file.write("add i" + "\n")
                self.file.write("conv i a" + "\n")
                self.file.write("ind c" + "\n")
                self.file.write("ldc c 0" + "\n")
                self.file.write("equ c" + "\n")
                self.file.write("fjp " + read_string_label + "\n")

                self.file.write("ssp " + str(self.symbol_table.next_variable_offset()) + "\n")

    def compile_scanf(self, function_call):
        specifier = re.search("%[disc]", function_call.parameter_list[0].primary)
        specifier_type = function_call.parameter_list[0].primary[specifier.start():specifier.end()][1]

        if specifier_type == "d" or specifier_type == "i":
            self.evaluate_expression(function_call.parameter_list[1])
            self.file.write("in i" + "\n")
            self.file.write("sto i" + "\n")

        elif specifier_type == "c":
            self.evaluate_expression(function_call.parameter_list[1])
            self.file.write("in c" + "\n")
            self.file.write("sto c" + "\n")

        else: # if specifier[1] == "s"
            self.evaluate_expression(function_call.parameter_list[1])
            self.file.write("ldc i -1" + "\n")

            scanf_label = self.get_unique_label_name("scanf_label")
            self.file.write(scanf_label + ":" + "\n")

            self.file.write("ldc i 1" + "\n")
            self.file.write("add i" + "\n")
            self.file.write("lod a 0 " + str(self.symbol_table.next_variable_offset()) + "\n")
            self.file.write("conv a i" + "\n")
            self.file.write("lod i 0 " + str(self.symbol_table.next_variable_offset() + 1) + "\n")
            self.file.write("add i" + "\n")
            self.file.write("conv i a" + "\n")
            self.file.write("in c" + "\n")
            self.file.write("sto c" + "\n")
            self.file.write("lod a 0 " + str(self.symbol_table.next_variable_offset()) + "\n")
            self.file.write("conv a i" + "\n")
            self.file.write("lod i 0 " + str(self.symbol_table.next_variable_offset() + 1) + "\n")
            self.file.write("add i" + "\n")
            self.file.write("conv i a" + "\n")
            self.file.write("ind c" + "\n")
            self.file.write("ldc c 27" + "\n")
            self.file.write("equ c" + "\n")
            self.file.write("fjp " + scanf_label + "\n")
            self.file.write("lod a 0 " + str(self.symbol_table.next_variable_offset()) + "\n")
            self.file.write("conv a i" + "\n")
            self.file.write("lod i 0 " + str(self.symbol_table.next_variable_offset() + 1) + "\n")
            self.file.write("add i" + "\n")
            self.file.write("conv i a" + "\n")
            self.file.write("ldc c 0" + "\n")
            self.file.write("sto c" + "\n")

            self.file.write("ssp " + str(self.symbol_table.next_variable_offset()) + "\n")

    def get_unique_label_name(self, base_label_name):
        self.latest_label_number += 1
        return base_label_name + "_" + str(self.latest_label_number)

