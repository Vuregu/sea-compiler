import copy

class ProgramNode:

    def __init__(self):
        self.global_statements = []

    def add_global_statement(self, statement):
        self.global_statements.append(statement)

class FunctionNode:

    def __init__(self, type, compound_statement):
        self.type = type
        self.id = None
        self.parameter_declarations = []
        self.compound_statement = compound_statement

    def set_id(self, id):
        self.id = id

    def add_parameter_declaration(self, parameter_declaration):
        self.parameter_declarations.append(parameter_declaration)

class TypeNode:

    def __init__(self, data_type):
        self.data_type = data_type
        self.pointers = []
        self.array_declaration = None

    def define_pointer(self, pointer):
        self.pointers.append(pointer)

    def declare_array(self, array_declaration):
        self.array_declaration = array_declaration

    def p_machine_type(self):
        if len(self.pointers) == 0:
            if self.data_type.data_type == "int":
                return "i"
            elif self.data_type.data_type == "char":
                return "c"
            elif self.data_type.data_type == "float":
                return "r"
        else:
            return "a"

    def p_machine_default_value(self):
        if self.data_type.data_type == "float":
            return "0.0"
        else:
            return "0"

    def p_machine_unit_value(self):
        if self.data_type.data_type == "float":
            return "1.0"
        else:
            return "1"

    def copy_type(self):
        data_type = TypeNode(DataTypeNode(self.data_type.data_type, self.data_type.is_constant))
        data_type.pointers = copy.deepcopy(self.pointers)

        if self.array_declaration is not None: data_type.pointers.append(PointerNode(False))
        return data_type

class DataTypeNode:

    def __init__(self, data_type = None, is_constant = False):
        self.data_type = data_type
        self.is_constant = is_constant

    def set_data_type(self, data_type):
        self.data_type = data_type

    def set_constant(self):
        self.is_constant = True

class PointerNode:

    def __init__(self, is_constant):
        self.is_constant = is_constant

class ArrayDeclarationNode:

    def __init__(self):
        self.size = None

    def set_size(self, expression):
        self.size = expression

class ArrayDereferenceNode:

    def __init__(self, expression):
        self.offset = expression

class ElementListNode:

    def __init__(self):
        self.elements = []

    def add_element(self, expression):
        self.elements.append(expression)

class ParameterDeclarationNode:

    def __init__(self, type):
        self.type = type
        self.id = None

    def set_id(self, id):
        self.id = id

class CompoundStatementNode:

    def __init__(self):
        self.local_statements = []

    def add_local_statement(self, statement):
        self.local_statements.append(statement)

class VariableDeclarationsNode:

    def __init__(self):
        self.variable_declarations = []

    def add_variable_declaration(self, variable_declaration):
        self.variable_declarations.append(variable_declaration)

class VariableDeclarationNode:

    def __init__(self, type):
        self.type = type
        self.id = None
        self.assignment = None

    def set_id(self, id):
        self.id = id

    def assign(self, expression):
        self.assignment = expression

class ConditionalStatementNode:

    def __init__(self, condition, statement):
        self.condition = condition
        self.statement = statement
        self.statement_else = None

    def set_statement_else(self, statement_else):
        self.statement_else = statement_else

class ForStatementNode:

    def __init__(self, initializer, condition, statement):
        self.initializer = initializer
        self.condition = condition
        self.increment = None
        self.statement = statement

    def set_increment(self, increment):
        self.increment = increment

class WhileStatementNode:

    def __init__(self, condition, statement):
        self.condition = condition
        self.statement = statement

class DoWhileStatementNode:

    def __init__(self, statement, condition):
        self.statement = statement
        self.condition = condition

class ExpressionStatementNode:

    def __init__(self, expression):
        self.expression = expression

class ReturnStatementNode:

    def __init__(self, expression):
        self.expression = expression

class ExpressionNode:

    def __init__(self, condition, expression_assignment):
        self.condition = condition
        self.expression_assignment = expression_assignment

class ConditionNode:

    def __init__(self, disjunction, ternary_true_expression, ternary_false_condition):
        self.disjunction = disjunction
        self.ternary_true_expression = ternary_true_expression
        self.ternary_false_condition = ternary_false_condition

class DisjunctionNode:

    def __init__(self, disjunction, conjunction):
        self.disjunction = disjunction
        self.conjunction = conjunction

class ConjunctionNode:

    def __init__(self, conjunction, relation):
        self.conjunction = conjunction
        self.relation = relation

class RelationNode:

    def __init__(self, addition, relation, with_addition):
        self.addition = addition
        self.relation = relation
        self.with_addition = with_addition

class AdditionNode:

    def __init__(self, addition, operation, with_multiplication):
        self.addition = addition
        self.operation = operation
        self.with_multiplication = with_multiplication

class MultiplicationNode:

    def __init__(self, multiplication, operation, with_factor):
        self.multiplication = multiplication
        self.operation = operation
        self.with_factor = with_factor

class FactorNode:

    def __init__(self, operation, factor):
        self.operation = operation
        self.factor = factor

class SecondaryNode:

    def __init__(self, secondary, operation):
        self.secondary = secondary
        self.operation = operation

class PrimaryNode:

    def __init__(self, primary, type):
        self.primary = primary
        self.type = type

class FunctionCallNode:

    def __init__(self, id):
        self.id = id
        self.parameter_list = []

    def add_parameter(self, expression):
        self.parameter_list.append(expression)

