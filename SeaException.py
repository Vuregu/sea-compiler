from antlr4.error.ErrorListener import ErrorListener
from SeaLexer import SeaLexer

class LexicalError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "Lexical Error: " + self.value + ""

class ParsingError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "Parsing Error: " + self.value + ""

class SemanticError(Exception):

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return "Semantic Error: " + self.value + ""

class SeaErrorListener(ErrorListener):

    def __init__(self):
        super().__init__()

    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        if type(recognizer) is SeaLexer:
            raise LexicalError("line " + str(line) + ":" + str(column) + " " + msg)
        else:
            raise ParsingError("line " + str(line) + ":" + str(column) + " " + msg)

