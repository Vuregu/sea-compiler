import SeaException
from SeaNodes import *

class VariableSymbol:

    def __init__(self, symbol_type, depth, offset):
        self.symbol_type = symbol_type
        self.depth = depth
        self.offset = offset

    def relative_depth(self, symbol_table):
        return symbol_table.depth - self.depth

class FunctionSymbol:

    def __init__(self, symbol_type, parameters, depth, procedure_name):
        self.symbol_type = symbol_type
        self.parameter_types = []
        for parameter in parameters:
            self.parameter_types.append(parameter.type)
        self.depth = depth
        self.procedure_name = procedure_name

    def relative_depth(self, symbol_table):
        return symbol_table.depth - self.depth

class SymbolTable:

    def __init__(self, scope, parent, depth, depth_name = None):
        self.entries = {}
        self.scope = scope
        self.parent = parent
        self.children = []
        self.depth = depth
        self.depth_name = depth_name
        self.variable_offset = 0

    def add_child_node(self, table_node):
        self.children.append(table_node)

    def get_child(self, scope):
        for child in self.children:
            if child.scope is scope:
                return child

    def resolve(self, symbol_name):
        symbol = self.find_entry(symbol_name)
        if symbol is not None:
            return symbol
        else:
            if self.parent is not None:
                return self.parent.resolve(symbol_name)
            else:
                return None

    def add_variable_entry(self, symbol_name, symbol_type):
        if symbol_name in self.entries and type(self.entries[symbol_name]) is VariableSymbol:
            raise SeaException.SemanticError("You have declared the variable \"" + symbol_name + "\" more than once in the same scope.")

        self.entries[symbol_name] = VariableSymbol(symbol_type, self.depth, self.next_variable_offset())
        self.variable_offset += 1 if symbol_type.array_declaration is None else symbol_type.array_declaration.size.primary

    def next_variable_offset(self):
        if self.parent is not None:
            return 5 + self.get_scope_offset()
        else:
            return self.variable_offset

    def get_scope_offset(self):
        return self.variable_offset + (self.parent.get_scope_offset() if self.parent.depth == self.depth else 0)

    def add_function_entry(self, symbol_name, symbol_type, parameters):
        if symbol_name in self.entries and type(self.entries[symbol_name]) is FunctionSymbol:
            raise SeaException.SemanticError("You have declared the function \"" + symbol_name + "\" more than once in the same scope.")

        self.entries[symbol_name] = FunctionSymbol(symbol_type, parameters, self.depth, self.get_depth_name() + symbol_name)

    def get_depth_name(self):
        return (self.parent.get_depth_name() if self.parent is not None else "") + (self.depth_name + "_" if self.depth_name is not None else "")

    def find_entry(self, symbol_name):
        try:
            return self.entries[symbol_name]
        except KeyError:
            return None

def build_symbol_table(abstract_tree):
    global_symbol_table = SymbolTable(None, None, 0)

    for statement in abstract_tree.global_statements:
        if type(statement) is VariableDeclarationsNode:

            for variable_declaration in statement.variable_declarations:
                if variable_declaration.type.array_declaration is not None:
                    validate_array(variable_declaration.type, variable_declaration.assignment)

                global_symbol_table.add_variable_entry(variable_declaration.id, variable_declaration.type)

        elif type(statement) is FunctionNode:

            validate_parameters(statement.id, statement.parameter_declarations)
            global_symbol_table.add_function_entry(statement.id, statement.type, statement.parameter_declarations)
            build_symbol_table_for_compound_statement(statement.id, statement.parameter_declarations, statement.compound_statement, global_symbol_table)

    return global_symbol_table

def validate_array(array_type, assignment):
    if assignment is None:
        if type(array_type.array_declaration.size) is not PrimaryNode or array_type.array_declaration.size.type != "int" or array_type.array_declaration.size.primary < 1:
            raise SeaException.SemanticError("Only static arrays are supported, please specify a constant array size that is >= 1.")

    elif type(assignment) is ElementListNode:
        if array_type.array_declaration.size is None:
            if len(assignment.elements) < 1:
                raise SeaException.SemanticError("Array needs at least one element.")
            else:
                array_type.array_declaration.size = PrimaryNode(len(assignment.elements), "int")

        elif type(array_type.array_declaration.size) is not PrimaryNode or array_type.array_declaration.size.type != "int" or array_type.array_declaration.size.primary < 1:
            raise SeaException.SemanticError("Only static arrays are supported, please specify a constant array size that is >= 1.")

    else:
        raise SeaException.SemanticError("Invalid array initialization, use {} to specify an element list.")

def validate_parameters(function_name, parameter_declarations):
    for parameter_declaration in parameter_declarations:
        if parameter_declaration.type.data_type.data_type == "void":
            if len(parameter_declarations) != 1:
                raise SeaException.SemanticError("\"void\" must be the only parameter in function \"" + function_name + "\".")

            else:
                if parameter_declaration.type.data_type.is_constant is True:
                    raise SeaException.SemanticError("\"void\" as the only parameter may not be qualified in function \"" + function_name + "\".")

                elif len(parameter_declaration.type.pointers) != 0:
                    raise SeaException.SemanticError("\"void\" cannot be a pointer in function \"" + function_name + "\".")

                elif parameter_declaration.type.array_declaration is not None:
                    raise SeaException.SemanticError("\"void\" cannot be an array in function \"" + function_name + "\".")

            del parameter_declarations[0]

        else:
            if parameter_declaration.id is None:
                raise SeaException.SemanticError("\"" + parameter_declaration.type.data_type.data_type + "\" does not have a name in function \"" + function_name + "\".")

def build_symbol_table_for_compound_statement(function_name, parameter_declarations, compound_statement, symbol_table):
    local_symbol_table = SymbolTable(compound_statement, symbol_table, symbol_table.depth + 1 if function_name is not None else symbol_table.depth, function_name)

    if function_name is not None:

        for parameter_declaration in parameter_declarations:
            if parameter_declaration.type.array_declaration is not None:
                parameter_declaration.type.array_declaration = None
                parameter_declaration.type.define_pointer(PointerNode(False))

            local_symbol_table.add_variable_entry(parameter_declaration.id, parameter_declaration.type)

    for statement in compound_statement.local_statements:
        if type(statement) is VariableDeclarationsNode:

            for variable_declaration in statement.variable_declarations:
                if variable_declaration.type.array_declaration is not None:
                    validate_array(variable_declaration.type, variable_declaration.assignment)

                local_symbol_table.add_variable_entry(variable_declaration.id, variable_declaration.type)

        elif type(statement) is FunctionNode:

            validate_parameters(statement.id, statement.parameter_declarations)
            local_symbol_table.add_function_entry(statement.id, statement.type, statement.parameter_declarations)
            build_symbol_table_for_compound_statement(statement.id, statement.parameter_declarations, statement.compound_statement, local_symbol_table)

        elif type(statement) is CompoundStatementNode:
            build_symbol_table_for_compound_statement(None, None, statement, local_symbol_table)

        elif type(statement) is ConditionalStatementNode:
            find_compound_statement(statement, local_symbol_table)

        elif type(statement) is ForStatementNode:
            find_compound_statement(statement, local_symbol_table)

        elif type(statement) is WhileStatementNode:
            find_compound_statement(statement, local_symbol_table)

        elif type(statement) is DoWhileStatementNode:
            find_compound_statement(statement, local_symbol_table)

    symbol_table.add_child_node(local_symbol_table)

def find_compound_statement(statement, symbol_table):
    if type(statement) is ConditionalStatementNode:
        find_compound_statement_in_sub_statement(statement.statement, symbol_table)
        find_compound_statement_in_sub_statement(statement.statement_else, symbol_table)

    elif type(statement) is ForStatementNode:
        if type(statement.initializer) is VariableDeclarationsNode:
            local_symbol_table = SymbolTable(statement.initializer, symbol_table, symbol_table.depth)

            for variable_declaration in statement.initializer.variable_declarations:
                if variable_declaration.type.array_declaration is not None:
                    validate_array(variable_declaration.type, variable_declaration.assignment)

                local_symbol_table.add_variable_entry(variable_declaration.id, variable_declaration.type)

            symbol_table.add_child_node(local_symbol_table)
            find_compound_statement_in_sub_statement(statement.statement, local_symbol_table)
        else:
            find_compound_statement_in_sub_statement(statement.statement, symbol_table)

    elif type(statement) is WhileStatementNode:
        find_compound_statement_in_sub_statement(statement.statement, symbol_table)

    elif type(statement) is DoWhileStatementNode:
        find_compound_statement_in_sub_statement(statement.statement, symbol_table)

def find_compound_statement_in_sub_statement(statement, symbol_table):
    if type(statement) is not ExpressionStatementNode and type(statement) is not ReturnStatementNode:

        if type(statement) is CompoundStatementNode:
            build_symbol_table_for_compound_statement(None, None, statement, symbol_table)
        else:
            find_compound_statement(statement, symbol_table)

def traverse_tree(abstract_tree, symbol_table):
    for statement in abstract_tree.global_statements:

        if type(statement) is VariableDeclarationsNode:
            traverse_variable_declarations(statement, symbol_table)

        elif type(statement) is FunctionNode:
            traverse_function(statement, symbol_table)

def traverse_variable_declarations(variable_declarations_node, symbol_table_node):
    for variable_declaration in variable_declarations_node.variable_declarations:

        if variable_declaration.assignment is not None:
            resolve_expression(variable_declaration.assignment, symbol_table_node)

def traverse_function(function_node, symbol_table_node):
    traverse_compound_statement(function_node.compound_statement, symbol_table_node)

def traverse_compound_statement(compound_statement_node, symbol_table_node):
    symbol_table_node = symbol_table_node.get_child(compound_statement_node)

    for statement in compound_statement_node.local_statements:

        if type(statement) is VariableDeclarationsNode:
            traverse_variable_declarations(statement, symbol_table_node)

        elif type(statement) is FunctionNode:
            traverse_function(statement, symbol_table_node)

        else:
            traverse_statement(statement, symbol_table_node)

def traverse_statement(statement_node, symbol_table_node):
    if type(statement_node) is CompoundStatementNode:
        traverse_compound_statement(statement_node, symbol_table_node)

    elif type(statement_node) is ConditionalStatementNode:
        resolve_expression(statement_node.condition, symbol_table_node)
        traverse_statement(statement_node.statement, symbol_table_node)

        if statement_node.statement_else is not None:
            traverse_statement(statement_node.statement_else, symbol_table_node)

    elif type(statement_node) is ForStatementNode:
        if type(statement_node.initializer) is VariableDeclarationsNode:
            symbol_table_node = symbol_table_node.get_child(statement_node.initializer)
            traverse_variable_declarations(statement_node.initializer, symbol_table_node)
        else:
            resolve_expression(statement_node.initializer, symbol_table_node)

        resolve_expression(statement_node.condition, symbol_table_node)

        if statement_node.increment is not None:
            resolve_expression(statement_node.increment, symbol_table_node)

        traverse_statement(statement_node.statement, symbol_table_node)

    elif type(statement_node) is WhileStatementNode:
        resolve_expression(statement_node.condition, symbol_table_node)
        traverse_statement(statement_node.statement, symbol_table_node)

    elif type(statement_node) is DoWhileStatementNode:
        traverse_statement(statement_node.statement, symbol_table_node)
        resolve_expression(statement_node.condition, symbol_table_node)

    elif type(statement_node) is ExpressionStatementNode:
        resolve_expression(statement_node.expression, symbol_table_node)

    elif type(statement_node) is ReturnStatementNode:
        resolve_expression(statement_node.expression, symbol_table_node)

def resolve_expression(expression, symbol_table_node):

    if type(expression) is ExpressionNode:
        resolve_expression(expression.condition, symbol_table_node)
        resolve_expression(expression.expression_assignment, symbol_table_node)

    elif type(expression) is ConditionNode:
        resolve_expression(expression.disjunction, symbol_table_node)
        resolve_expression(expression.ternary_true_expression, symbol_table_node)
        resolve_expression(expression.ternary_false_condition, symbol_table_node)

    elif type(expression) is DisjunctionNode:
        resolve_expression(expression.disjunction, symbol_table_node)
        resolve_expression(expression.conjunction, symbol_table_node)

    elif type(expression) is ConjunctionNode:
        resolve_expression(expression.conjunction, symbol_table_node)
        resolve_expression(expression.relation, symbol_table_node)

    elif type(expression) is RelationNode:
        resolve_expression(expression.addition, symbol_table_node)
        resolve_expression(expression.with_addition, symbol_table_node)

    elif type(expression) is AdditionNode:
        resolve_expression(expression.addition, symbol_table_node)
        resolve_expression(expression.with_multiplication, symbol_table_node)

    elif type(expression) is MultiplicationNode:
        resolve_expression(expression.multiplication, symbol_table_node)
        resolve_expression(expression.with_factor, symbol_table_node)

    elif type(expression) is FactorNode:
        resolve_expression(expression.factor, symbol_table_node)

    elif type(expression) is SecondaryNode:
        resolve_expression(expression.secondary, symbol_table_node)

    elif type(expression) is PrimaryNode:
        if expression.type == "id":
            symbol = symbol_table_node.resolve(expression.primary)
            if symbol is None:
                raise SeaException.SemanticError("Cannot resolve the variable \"" + expression.primary + "\"" + ".")

            elif type(symbol) is FunctionSymbol:
                raise SeaException.SemanticError("\"" + expression.primary + "\"" + " is a function, expecting a function call.")

    elif type(expression) is FunctionCallNode and expression.id != "printf" and expression.id != "scanf":
        symbol = symbol_table_node.resolve(expression.id)
        if symbol is None:
            raise SeaException.SemanticError("Cannot resolve the function \"" + expression.id + "\"" + ".")

        elif type(symbol) is VariableSymbol:
            raise SeaException.SemanticError("\"" + expression.id + "\"" + " is a variable, cannot perform a function call.")

        elif len(symbol.parameter_types) != len(expression.parameter_list):
            raise SeaException.SemanticError("Expecting " + str(len(symbol.parameter_types)) + " parameter(s) for function \"" + expression.id + "\", you have provided " + str(len(expression.parameter_list)) + ".")

        for parameter in expression.parameter_list:
            resolve_expression(parameter, symbol_table_node)

