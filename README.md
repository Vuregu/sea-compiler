# Sea Compiler
_Version 1.0.0 - by Vreg Andreasyan_

## 1. Introduction
--------------------
Sea Compiler is a fast fully functional cross-platform C compiler written in Python. It supports a relatively large subset of C as can been seen in the working test files, and many existing sample programs that can be found on the internet can be compiled with little to no adjustments.

## 2. Quick start
--------------------
### 2.1 Compiling your C source
Run Sea Compiler by executing `main.py` using Python3. Provide the `.c` source files you want to compile as command-line arguments separated by spaces. Sea Compiler will then turn them into compiled `.p` files that you can execute using the virtual P machine that comes with Sea Compiler.

Sea Compiler requires the [ANTLR runtime](https://pypi.python.org/pypi/antlr4-python3-runtime) for parsing, so make sure that is present in your Python3 environment.

### 2.2 Running your compiled source
To execute your compiled `.p` files, you will have to use the virtual P machine that is provided with Sea Compiler. This virtual machine is written in C++ and comes with its uncompiled source, so it's up to you to compile into an executable for your target platform using your favorite C++ compiler. Once you have an executable for your target platform, you can run your compiled `.p` files on that platform by providing them as command-line arguments to your virtual P machine executable.

## 3. License and copyright
--------------------
Sea Compiler is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Sea Compiler is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Sea Compiler.  If not, see <http://www.gnu.org/licenses/>.

Copyright (c) 2016, Vreg Andreasyan <vreg.andreasyan@gmail.com\>. All rights reserved.