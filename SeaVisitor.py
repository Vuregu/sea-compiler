import antlr4
from SeaNodes import *
from SeaParser import SeaParser

class SeaVisitor:

    # Visit a parse tree produced by program.
    def visitProgram(self, ctx:SeaParser.ProgramContext):
        program = ProgramNode()

        for child in ctx.getChildren():
            if type(child) is SeaParser.Variable_declarationsContext:
                program.add_global_statement(self.visitVariable_declarations(child))

            elif type(child) is SeaParser.FunctionContext:
                program.add_global_statement(self.visitFunction(child))

        return program

    # Visit a parse tree produced by function.
    def visitFunction(self, ctx:SeaParser.FunctionContext):
        function = FunctionNode(TypeNode(self.visitType_specifier(ctx.getChild(0))), self.visitCompound_statement(ctx.getChild(2)))
        self.visitFunction_declaration(ctx.getChild(1), function)

        return function

    # Visit a parse tree produced by function_declaration.
    def visitFunction_declaration(self, ctx:SeaParser.Function_declarationContext, function):

        for child in ctx.getChildren():
            if type(child) is SeaParser.PointerContext:
                function.type.define_pointer(self.visitPointer(child))

            elif type(child) is antlr4.tree.Tree.TerminalNodeImpl and child.symbol.type == 46:
                function.set_id(str(child))

            elif type(child) is SeaParser.Parameter_declarationsContext:
                for parameter_declaration in child.getChildren():
                    if type(parameter_declaration) is SeaParser.Parameter_declarationContext:
                        function.add_parameter_declaration(self.visitParameter_declaration(parameter_declaration))

    # Visit a parse tree produced by type_specifier.
    @staticmethod
    def visitType_specifier(ctx:SeaParser.Type_specifierContext):
        data_type = DataTypeNode()

        for child in ctx.getChildren():
            if type(child) is SeaParser.Data_typeContext:
                data_type.set_data_type(str(child.getChild(0)))

            elif type(child) is antlr4.tree.Tree.TerminalNodeImpl and child.symbol.type == 9:
                data_type.set_constant()

        return data_type

    # Visit a parse tree produced by pointer.
    @staticmethod
    def visitPointer(ctx:SeaParser.PointerContext):
        return PointerNode(True if ctx.getChildCount() == 2 else False)

    # Visit a parse tree produced by array_declaration.
    def visitArray_declaration(self, ctx:SeaParser.Array_declarationContext):
        array_declaration = ArrayDeclarationNode()

        if ctx.getChildCount() == 3:
            array_declaration.set_size(self.visitExpression(ctx.getChild(1)))

        return array_declaration

    # Visit a parse tree produced by array_dereference.
    def visitArray_dereference(self, ctx:SeaParser.Array_dereferenceContext):
        return ArrayDereferenceNode(self.visitExpression(ctx.getChild(1)))

    # Visit a parse tree produced by element_list.
    def visitElement_list(self, ctx:SeaParser.Element_listContext):
        element_list = ElementListNode()

        for child in ctx.getChildren():
            if type(child) is SeaParser.ExpressionContext:
                element_list.add_element(self.visitExpression(child))

        return element_list

    # Visit a parse tree produced by parameter_declaration.
    def visitParameter_declaration(self, ctx:SeaParser.Parameter_declarationContext):
        parameter_declaration = ParameterDeclarationNode(TypeNode(self.visitType_specifier(ctx.getChild(0))))

        for child in ctx.getChildren():
            if type(child) is SeaParser.PointerContext:
                parameter_declaration.type.define_pointer(self.visitPointer(child))

            elif type(child) is antlr4.tree.Tree.TerminalNodeImpl and child.symbol.type == 46:
                parameter_declaration.set_id(str(child))

            elif type(child) is SeaParser.Array_declarationContext:
                parameter_declaration.type.declare_array(self.visitArray_declaration(child))

        return parameter_declaration

    # Visit a parse tree produced by compound_statement.
    def visitCompound_statement(self, ctx:SeaParser.Compound_statementContext):
        compound_statement = CompoundStatementNode()

        for child in ctx.getChildren():
            if type(child) is SeaParser.Variable_declarationsContext:
                compound_statement.add_local_statement(self.visitVariable_declarations(child))

            elif type(child) is SeaParser.FunctionContext:
                compound_statement.add_local_statement(self.visitFunction(child))

            elif type(child) is SeaParser.StatementContext:
                compound_statement.add_local_statement(self.visitStatement(child))

        return compound_statement

    # Visit a parse tree produced by variable_declarations.
    def visitVariable_declarations(self, ctx:SeaParser.Variable_declarationsContext):
        variable_declarations = VariableDeclarationsNode()
        variable_type = TypeNode(self.visitType_specifier(ctx.getChild(0)))

        for child in ctx.getChildren():
            if type(child) is SeaParser.Variable_declarationContext:
                variable_declarations.add_variable_declaration(self.visitVariable_declaration(child, variable_type))

        return variable_declarations

    # Visit a parse tree produced by variable_declaration.
    def visitVariable_declaration(self, ctx:SeaParser.Variable_declarationContext, variable_type):
        variable_declaration = VariableDeclarationNode(variable_type)

        for child in ctx.getChildren():
            if type(child) is SeaParser.PointerContext:
                variable_declaration.type.define_pointer(self.visitPointer(child))

            elif type(child) is antlr4.tree.Tree.TerminalNodeImpl and child.symbol.type == 46:
                variable_declaration.set_id(str(child))

            elif type(child) is SeaParser.Array_declarationContext:
                variable_declaration.type.declare_array(self.visitArray_declaration(child))

            elif type(child) is SeaParser.ExpressionContext:
                variable_declaration.assign(self.visitExpression(child))

            elif type(child) is SeaParser.Element_listContext:
                variable_declaration.assign(self.visitElement_list(child))

        return variable_declaration

    # Visit a parse tree produced by statement.
    def visitStatement(self, ctx:SeaParser.StatementContext):
        statement = ctx.getChild(0)

        if type(statement) is SeaParser.Compound_statementContext:
            return self.visitCompound_statement(statement)

        elif type(statement) is SeaParser.Conditional_statementContext:
            return self.visitConditional_statement(statement)

        elif type(statement) is SeaParser.For_statementContext:
            return self.visitFor_statement(statement)

        elif type(statement) is SeaParser.While_statementContext:
            return self.visitWhile_statement(statement)

        elif type(statement) is SeaParser.Do_while_statementContext:
            return self.visitDo_while_statement(statement)

        elif type(statement) is SeaParser.Expression_statementContext:
            return self.visitExpression_statement(statement)

        elif type(statement) is SeaParser.Return_statementContext:
            return self.visitReturn_statement(statement)

    # Visit a parse tree produced by conditional_statement.
    def visitConditional_statement(self, ctx:SeaParser.Conditional_statementContext):
        conditional_statement = ConditionalStatementNode(self.visitExpression(ctx.getChild(2)), self.visitStatement(ctx.getChild(4)))

        if ctx.getChildCount() == 7:
            conditional_statement.set_statement_else(self.visitStatement(ctx.getChild(6)))

        return conditional_statement

    # Visit a parse tree produced by for_statement.
    def visitFor_statement(self, ctx:SeaParser.For_statementContext):
        for_statement = ForStatementNode(self.visitExpression(ctx.getChild(2).getChild(0)) if ctx.getChild(2).getChildCount() == 2 else self.visitVariable_declarations(ctx.getChild(2).getChild(0)), self.visitExpression(ctx.getChild(3).getChild(0)), self.visitStatement(ctx.getChild(ctx.getChildCount() - 1)))

        if ctx.getChildCount() == 7:
            for_statement.set_increment(self.visitExpression(ctx.getChild(4).getChild(0)))

        return for_statement

    # Visit a parse tree produced by while_statement.
    def visitWhile_statement(self, ctx:SeaParser.While_statementContext):
        return WhileStatementNode(self.visitExpression(ctx.getChild(2)), self.visitStatement(ctx.getChild(4)))

    # Visit a parse tree produced by do_while_statement.
    def visitDo_while_statement(self, ctx:SeaParser.Do_while_statementContext):
        return DoWhileStatementNode(self.visitStatement(ctx.getChild(1)), self.visitExpression(ctx.getChild(4)))

    # Visit a parse tree produced by expression_statement.
    def visitExpression_statement(self, ctx:SeaParser.Expression_statementContext):
        return ExpressionStatementNode(self.visitExpression(ctx.getChild(0)))

    # Visit a parse tree produced by return_statement.
    def visitReturn_statement(self, ctx: SeaParser.Return_statementContext):
        return ReturnStatementNode(self.visitExpression(ctx.getChild(1)) if ctx.getChildCount() == 3 else None)

    # Visit a parse tree produced by expression.
    def visitExpression(self, ctx:SeaParser.ExpressionContext):

        if ctx.getChildCount() == 3:
            return ExpressionNode(self.visitCondition(ctx.getChild(0)), self.visitExpression(ctx.getChild(2)))
        else:
            return self.visitCondition(ctx.getChild(0))

    # Visit a parse tree produced by condition.
    def visitCondition(self, ctx:SeaParser.ConditionContext):
        if ctx.getChildCount() == 5:
            return ConditionNode(self.visitDisjunction(ctx.getChild(0)), self.visitExpression(ctx.getChild(2)), self.visitCondition(ctx.getChild(4)))
        else:
            return self.visitDisjunction(ctx.getChild(0))

    # Visit a parse tree produced by disjunction.
    def visitDisjunction(self, ctx:SeaParser.DisjunctionContext):
        if ctx.getChildCount() == 3:
            return DisjunctionNode(self.visitDisjunction(ctx.getChild(0)), self.visitConjunction(ctx.getChild(2)))
        else:
            return self.visitConjunction(ctx.getChild(0))

    # Visit a parse tree produced by conjunction.
    def visitConjunction(self, ctx:SeaParser.ConjunctionContext):
        if ctx.getChildCount() == 3:
            return ConjunctionNode(self.visitConjunction(ctx.getChild(0)), self.visitRelation(ctx.getChild(2)))
        else:
            return self.visitRelation(ctx.getChild(0))

    # Visit a parse tree produced by relation.
    def visitRelation(self, ctx:SeaParser.RelationContext):
        if ctx.getChildCount() == 3:
            return RelationNode(self.visitAddition(ctx.getChild(0)), str(ctx.getChild(1)), self.visitAddition(ctx.getChild(2)))
        else:
            return self.visitAddition(ctx.getChild(0))

    # Visit a parse tree produced by addition.
    def visitAddition(self, ctx:SeaParser.AdditionContext):
        if ctx.getChildCount() == 3:
            return AdditionNode(self.visitAddition(ctx.getChild(0)), str(ctx.getChild(1)), self.visitMultiplication(ctx.getChild(2)))
        else:
            return self.visitMultiplication(ctx.getChild(0))

    # Visit a parse tree produced by multiplication.
    def visitMultiplication(self, ctx:SeaParser.MultiplicationContext):
        if ctx.getChildCount() == 3:
            return MultiplicationNode(self.visitMultiplication(ctx.getChild(0)), str(ctx.getChild(1)), self.visitFactor(ctx.getChild(2)))
        else:
            return self.visitFactor(ctx.getChild(0))

    # Visit a parse tree produced by factor.
    def visitFactor(self, ctx:SeaParser.FactorContext):
        if ctx.getChildCount() == 2:
            return FactorNode(str(ctx.getChild(0)), self.visitFactor(ctx.getChild(1)))
        else:
            return self.visitSecondary(ctx.getChild(0))

    # Visit a parse tree produced by secondary.
    def visitSecondary(self, ctx:SeaParser.SecondaryContext):
        if ctx.getChildCount() == 2:
            operation = ctx.getChild(1)
            if type(operation) is SeaParser.Array_dereferenceContext:
                return SecondaryNode(self.visitSecondary(ctx.getChild(0)), self.visitArray_dereference(operation))
            else:
                return SecondaryNode(self.visitSecondary(ctx.getChild(0)), str(operation))
        else:
           return self.visitPrimary(ctx.getChild(0))

    # Visit a parse tree produced by primary.
    def visitPrimary(self, ctx:SeaParser.PrimaryContext):
        if ctx.getChildCount() == 3:
            return self.visitExpression(ctx.getChild(1))
        else:
            primary = ctx.getChild(0)
            if type(primary) is antlr4.tree.Tree.TerminalNodeImpl:

                if primary.symbol.type == 42:
                    return PrimaryNode(float(str(primary)), "float")
                elif primary.symbol.type == 43:
                    return PrimaryNode(int(str(primary)), "int")
                elif primary.symbol.type == 44:
                    return PrimaryNode(str(primary)[1 : -1], "char")
                elif primary.symbol.type == 45:
                    return PrimaryNode(str(primary)[1 : -1], "string")
                elif primary.symbol.type == 46:
                    return PrimaryNode(str(primary), "id")
            else:
                return self.visitFunction_call(primary)

    # Visit a parse tree produced by function_call.
    def visitFunction_call(self, ctx:SeaParser.Function_callContext):
        function_call = FunctionCallNode(str(ctx.getChild(0)))

        for child in ctx.getChildren():
            if type(child) is SeaParser.ExpressionContext:
                function_call.add_parameter(self.visitExpression(child))

        return function_call

